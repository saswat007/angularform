const express = require("express");
const bodyparser = require("body-parser");
const cors = require("cors");

const port = 3000;
const app = express();
app.use(bodyparser.json());
app.use(cors());

app.get("/",(req,res) => {
    res.send("coming from server");
})

app.post("/register",(req,res)=>{
    console.log(req.body);
    res.status(200).send(req.body);
})

app.listen(port,()=>{
    console.log(`server running on localhost ${port}`);
})


