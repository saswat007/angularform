import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/user';
import {RegisterService} from "../Service/register.service";

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {

  minDate = new Date(1990 ,0 ,1);
  maxDate = new Date(2000,11, 31);
  errorMsg = '';

 // userModel = new User("soumya","jena","saumya@gmail.com",1234567890,"1/1/1990");
  userModel = new User("","","",null,"","default");
  topics = ["Angular","React","vuejs"];
 
  topicHaserror = false;
  submit = true;

  validateTopic(value){
   if(value==='default')
   {
     this.topicHaserror = true;
   } 
   else 
   {
     this.topicHaserror = false;
   }
  }
  onSubmit(){
    this._registerservice.registration(this.userModel).subscribe(
      data => console.log("success!",data),
      error => this.errorMsg = error.statusText
    )
    this.submit = false;
  }

  constructor(private _registerservice : RegisterService) { }

  ngOnInit(): void {
  }

}
