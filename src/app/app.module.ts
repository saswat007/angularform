import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './Mycomponent/navbar/navbar.component';
import { InputFormComponent } from './Mycomponent/input-form/input-form.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { HttpClientModule } from '@angular/common/http';
// import{ MatInput} from '@angular/material/input';
@NgModule({
  declarations: [AppComponent, NavbarComponent, InputFormComponent],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatNativeDateModule,
    MatDatepickerModule,
    HttpClientModule
    // MatInput
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
