export class User 
{
    constructor(
        public fname : string,
        public lname : string,
        public email : string,
        public mob : number,
        public dob : string,
        public topic : string
    ){

    }
}
